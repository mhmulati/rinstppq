/*! Some ideas taken from https://scip.zib.de/doc-3.1.0/examples/VRP/main__vrp_8cpp.php and https://scip.zib.de/doc-3.1.0/examples/VRP/main__vrp_8cpp_source.php */

#pragma once
#include <utilppq/types.h>
using namespace std;

enum class SpecKey {
	NONE,
	NAME,
	INST_TYPE,
	COMMENT,
	DIMENSION,
	CAPACITY,
	EDGE_WEIGHT_TYPE,
	EDGE_WEIGHT_FORMAT,
	EDGE_DATA_FORMAT,
	NODE_COORD_TYPE,
	DISPLAY_DATA_TYPE,
	VEHICLES_NMAN,
	DISTANCE_NMAN,
	SERVICE_TIME_NMAN,
	CAPACITY_FACTOR_NMAN
};
const vector<string> SpecKey_ = {
	"",
	"NAME",
	"TYPE",
	"COMMENT",
	"DIMENSION",
	"CAPACITY",
	"EDGE_WEIGHT_TYPE",
	"EDGE_WEIGHT_FORMAT",
	"EDGE_DATA_FORMAT",
	"NODE_COORD_TYPE",
	"DISPLAY_DATA_TYPE",
	"VEHICLES",
	"DISTANCE",
	"SERVICE_TIME",
	"CAPACITY_FACTOR"
};

enum class           InstType    {
	NONE,
	CVRP,
	TSP
};
const vector<string> InstType_ = {
	"",
	"CVRP",
	"TSP"
};

enum class           EdgeWeightType    {
	NONE,
	EUC_2D,
	EXPLICIT,
	ALIEN_FUNCTION,
	GEO
};
const vector<string> EdgeWeightType_ = {
	"",
	"EUC_2D",
	"EXPLICIT",
	"FUNCTION",
	"GEO"
};

enum class           EdgeWeightFormat    {
	NONE,
	FUNCTION,
	LOWER_ROW,
	ALIEN_EUC_2D,
	UPPER_ROW,
	FULL_MATRIX,
	LOWER_DIAG_ROW
};
const vector<string> EdgeWeightFormat_ = {
	"",
	"FUNCTION",
	"LOWER_ROW",
	"EUC_2D",
	"UPPER_ROW",
	"FULL_MATRIX",
	"LOWER_DIAG_ROW"
};

enum class           EdgeDataFormat    {
	NONE
};
const vector<string> EdgeDataFormat_ = {
	""
};

enum class           NodeCoordType    {
	NONE,
	TWOD_COORDS,
	NO_COORDS,
	ALIEN_GEO_COORDS
};
const vector<string> NodeCoordType_ = {
	"",
	"TWOD_COORDS",
	"NO_COORDS",
	"GEO_COORDS"
};

enum class           DisplayDataType    {
	NONE,
	COORD_DISPLAY,
	NO_DISPLAY,
	TWOD_DISPLAY
};
const vector<string> DisplayDataType_ = {
	"",
	"COORD_DISPLAY",
	"NO_DISPLAY",
	"TWOD_DISPLAY"
};

class Inst {
public:
	string           _id;  const string& _name = _id;
	string           _comment;
	InstType         _inst_type;
	int              _n;   const int& _dimension = _n;
	int              _num_customers;
	int              _num_depots;
	double           _Q;   const double&         _cap =          _Q;
	int     _discrete_Q;   const int&   _discrete_cap = _discrete_Q;
	double           _sum_qs;
	int              _discrete_sum_qs;
	int              _k;  const int& _num_vehicles_NMAN = _k;
	double           _frac_lb_k;
	int              _discrete_lb_k;
	double           _q_factor_NMAN;  const double& _cap_demands_factor_NMAN = _q_factor_NMAN;
	double           _distance_NMAN;  // NMAN: not in manual
	double           _service_time_NMAN;
	EdgeWeightType   _edge_weight_type;
	EdgeWeightFormat _edge_weight_format;
	EdgeDataFormat   _edge_data_format;
	NodeCoordType    _node_coord_type;
	DisplayDataType  _display_data_type;
	vector<Coords<double>> _coords;
	vector<Coords<double>> _display_coords;
	vector<int>            _depots;
	vector<double>         _qs;  const vector<double>&          _demands =          _qs;
	vector<int>   _discrete_qs;  const vector<int>&    _discrete_demands = _discrete_qs;
	matrix<double>         _ds;  const matrix<double>&            _dists =          _ds;
	bool                   _int_dist_measure;
	int                    _num_dec_places_real_dist;
	bool                   _accept_only_int_capacity;
	bool                   _accept_only_int_client_demands;
	Inst();
	Inst(stringstream& data, const bool real_dist, const int num_dec_places_real_dist, const bool accept_only_int_capacity, const bool accept_only_int_client_demands);
	Inst(stringstream& data, const int extern_k, const double extern_rho, const double extern_q0, const bool override_inst_k, const bool override_inst_q0, const bool real_dist, const int num_dec_places_real_dist, const bool accept_only_int_capacity, const bool accept_only_int_client_demands);
	Inst(const string& instfn, const int extern_k, const double extern_rho, const double extern_q0, const bool override_inst_k, const bool override_inst_q0, const bool real_dist, const int num_dec_places_real_dist, const bool accept_only_int_capacity, const bool accept_only_int_client_demands, const bool try_stdin);
	void recgn(stringstream&);
	void recgn_spec(stringstream&);
	void recgn_spec_key(SpecKey, string&);
	void recgn_data(stringstream&);
	void recgn_node_coord_section_content(stringstream&);
	void recgn_edge_weight_section_content(stringstream&);
	void recgn_demand_section_content(stringstream&);
	void recgn_depot_section_content(stringstream&);
	void recgn_display_data_section_content(stringstream&);
	void calc_dists();
	void calc_intern_data();
	void calc_and_check_extern_data (const int extern_k, const bool override_inst_k, const double extern_rho, const double extern_q0, const bool override_inst_q0);
	void fill_discrete();
	double get_dist_canonical_route();
	const string as_str();
private:
};
