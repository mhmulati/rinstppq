/*@{*/// header
#include "inst.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <limits>
#include <utility>
#include <cmath>
#include <limits>
#include <boost/algorithm/string.hpp>
#include <utilppq/prepr.h>
#include <utilppq/util.h>
#include <utilppq/consts.h>
using namespace std;
/*@}*/

// #include <utilppq/ACTSELLOG>


Inst::Inst() {
	LGN("+ " << __PRETTY_FUNCTION__);
	_id         = "";
	_comment    = "";
	_inst_type  = InstType::NONE;
	_n               = 0;
	_num_customers   = 0;
	_num_depots      = 0;
	_Q               = 0.0;
	_discrete_Q      = 0;
	_sum_qs          = 0.0;
	_discrete_sum_qs = 0;
	_k               = 0;
	_frac_lb_k       = 0.0;
	_discrete_lb_k   = 0;
	_q_factor_NMAN   = 1.0;
	_distance_NMAN      = numeric_limits<double>::infinity();
	_service_time_NMAN  = 0.0;
	_edge_weight_type   = EdgeWeightType::NONE;
	_edge_weight_format = EdgeWeightFormat::NONE;
	_edge_data_format   = EdgeDataFormat::NONE;
	_node_coord_type    = NodeCoordType::NO_COORDS;
	_display_data_type  = DisplayDataType::NONE;
	_int_dist_measure               = true;
	_num_dec_places_real_dist       = 0;
	_accept_only_int_capacity       = true;
	_accept_only_int_client_demands = true;
	LGN("- " << __PRETTY_FUNCTION__);
}

Inst::Inst(stringstream& data,
const bool real_dist=false,
const int num_dec_places_real_dist=0,
const bool accept_only_int_capacity=true,
const bool accept_only_int_client_demands=true) :
Inst() {
	_int_dist_measure               = not real_dist;
	_num_dec_places_real_dist       = num_dec_places_real_dist;
	_accept_only_int_capacity       = accept_only_int_capacity;
	_accept_only_int_client_demands = accept_only_int_client_demands;

	recgn(data);
	calc_dists();
	calc_intern_data();
	fill_discrete();

	LGN(as_str());
}

Inst::Inst(stringstream& data,
const int extern_k,
const double extern_rho,
const double extern_q0,
const bool override_inst_k=true,
const bool override_inst_q0=true,
const bool real_dist=false,
const int num_dec_places_real_dist=0,
const bool accept_only_int_capacity=true,
const bool accept_only_int_client_demands=true) :
Inst(data, real_dist, num_dec_places_real_dist, accept_only_int_capacity, accept_only_int_client_demands) {
	calc_and_check_extern_data(extern_k, override_inst_k, extern_rho, extern_q0, override_inst_q0);
}

Inst::Inst(const string& instfn,
const int extern_k,
const double extern_rho,
const double extern_q0,
const bool override_inst_k=true,
const bool override_inst_q0=true,
const bool real_dist=false,
const int num_dec_places_real_dist=0,
const bool accept_only_int_capacity=true,
const bool accept_only_int_client_demands=true,
const bool try_stdin=false) :
Inst() {
	LGN("+ " << __PRETTY_FUNCTION__);
	_int_dist_measure               = not real_dist;
	_num_dec_places_real_dist       = num_dec_places_real_dist;
	_accept_only_int_capacity       = accept_only_int_capacity;
	_accept_only_int_client_demands = accept_only_int_client_demands;

	stringstream data;
	get_data_from_stdin_or_file(instfn, data, try_stdin);

	recgn(data);			LGN("recgn OK");
	calc_dists();			LGN("calc_dists OK");
	calc_intern_data();		LGN("calc_intern_data OK");

	calc_and_check_extern_data(extern_k, override_inst_k, extern_rho, extern_q0, override_inst_q0);
						LGN("calc_and_check_extern_data OK");
	fill_discrete();			LGN("fill_discrete OK");

	LGN(as_str());
	LGN("- " << __PRETTY_FUNCTION__);
}

void Inst::recgn(stringstream& data) {
	recgn_spec(data);
	recgn_data(data);
}

void Inst::recgn_spec(stringstream& data) {
	// In any order
	int i;
	string line, key, val;
	SpecKey code = SpecKey::NONE;
	bool is_key_valid_for_spec = true;
	while (is_key_valid_for_spec && getline(data, line="")) {
		i = line.find_first_of(':');
		key = line.substr(0, i);
		val = line.substr(i + 1);
		boost::trim(key);
		boost::trim(val);
		code = to_code<SpecKey>(key, SpecKey_);
		is_key_valid_for_spec = (i != (int) string::npos);
		if (is_key_valid_for_spec)
			recgn_spec_key(code, val);
		else if (line.length())
			putback(data, line + '\n');
	}
	// Check and set some defaults
	if (_edge_weight_type == EdgeWeightType::EXPLICIT) {
		proceed_only_if_(_edge_weight_format != EdgeWeightFormat::NONE);
		if (_display_data_type == DisplayDataType::NONE)
			_display_data_type = DisplayDataType::NO_DISPLAY;
	}
	else if (_edge_weight_type == EdgeWeightType::ALIEN_FUNCTION) {
		proceed_only_if_(_edge_weight_format == EdgeWeightFormat::ALIEN_EUC_2D and _node_coord_type == NodeCoordType::TWOD_COORDS);
		_display_data_type = DisplayDataType::COORD_DISPLAY;
	}
	else {  // implicit
		proceed_only_if_(_edge_weight_format == EdgeWeightFormat::NONE or _edge_weight_format == EdgeWeightFormat::FUNCTION);
		proceed_only_if_(_node_coord_type == NodeCoordType::NONE or _node_coord_type == NodeCoordType::TWOD_COORDS or _node_coord_type == NodeCoordType::NO_COORDS);

		if (_edge_weight_type == EdgeWeightType::EUC_2D)
			_node_coord_type = NodeCoordType::TWOD_COORDS;
		else if (_edge_weight_type == EdgeWeightType::GEO and _node_coord_type == NodeCoordType::NO_COORDS)
			_node_coord_type = NodeCoordType::ALIEN_GEO_COORDS;

		if (_display_data_type == DisplayDataType::NONE)
			_display_data_type = DisplayDataType::COORD_DISPLAY;
	}
}

void Inst::recgn_spec_key(SpecKey code, string& val) {
	switch (code) {
	case SpecKey::NAME:
		_id = val;
		break;
	case SpecKey::COMMENT:
		_comment = val;
		break;
	case SpecKey::INST_TYPE:
		_inst_type = to_code<InstType>(val, InstType_);
		fail_if_(_inst_type != InstType::CVRP and _inst_type != InstType::TSP);
		break;
	case SpecKey::DIMENSION:
		_n = stoi(val);
		_ds.assign(_n, vector<double>(_n, 0.0));
		break;
	case SpecKey::CAPACITY:
		_Q = stof(val);
		proceed_only_if_(_accept_only_int_capacity and is_int(_Q, EPS), "Error: Inst: recgn_spec_key: " + from_code<SpecKey>(code, SpecKey_) + " is not integer");
		break;
	case SpecKey::VEHICLES_NMAN:
		_k = stoi(val);
		break;
	case SpecKey::DISTANCE_NMAN:
		_distance_NMAN = stof(val);
		break;
	case SpecKey::CAPACITY_FACTOR_NMAN:
		_q_factor_NMAN = stof(val);
		break;
	case SpecKey::SERVICE_TIME_NMAN:
		_service_time_NMAN = stof(val);
		break;
	case SpecKey::EDGE_WEIGHT_TYPE:
		_edge_weight_type = to_code<EdgeWeightType>(val, EdgeWeightType_);
		break;
	case SpecKey::EDGE_WEIGHT_FORMAT:
		_edge_weight_format = to_code<EdgeWeightFormat>(val, EdgeWeightFormat_);
		break;
	case SpecKey::EDGE_DATA_FORMAT:
		_edge_data_format = to_code<EdgeDataFormat>(val, EdgeDataFormat_);
		fail("Error: Inst::recgn_spec_key: spec key not implemented: " + from_code<SpecKey>(SpecKey::EDGE_DATA_FORMAT, EdgeDataFormat_));
		break;
	case SpecKey::NODE_COORD_TYPE:
		_node_coord_type = to_code<NodeCoordType>(val, NodeCoordType_);
		break;
	case SpecKey::DISPLAY_DATA_TYPE:
		_display_data_type = to_code<DisplayDataType>(val, DisplayDataType_);
		break;
	case SpecKey::NONE:
		break;
	default:
		fail("Error: Inst::recgn_spec_key: spec key not found");
	}
}

void Inst::recgn_data(stringstream& data) {
	LGN("+ " << __PRETTY_FUNCTION__);
	bool got_coords_or_weights = false;
	bool got_demands           = false;
	bool got_depots            = false;
	bool got_displays          = false;
	bool reached_eof_flag      = false;
	// In any order
	string line;
	string uniq_err_msg = "Error: Inst::recgn_data: trying to get edge weights again: ";
	bool is_id_valid = true;
	while (not reached_eof_flag and is_id_valid and getline(data, line="")) {
		boost::trim(line);
		LGN("line " << line);
		if (line == "NODE_COORD_SECTION") {
			proceed_only_if_(_edge_weight_type != EdgeWeightType::EXPLICIT);
			turn_true_only_once_or_fail(got_coords_or_weights, uniq_err_msg + line);
			recgn_node_coord_section_content(data);
		}
		else if (line == "EDGE_WEIGHT_SECTION") {
			proceed_only_if_(_edge_weight_type == EdgeWeightType::EXPLICIT);
			turn_true_only_once_or_fail(got_coords_or_weights, uniq_err_msg + line);
			recgn_edge_weight_section_content(data);
		}
		else if (line == "DEMAND_SECTION") {
			turn_true_only_once_or_fail(got_demands, uniq_err_msg + line);
			recgn_demand_section_content(data);
		}
		else if (line == "DEPOT_SECTION") {
			turn_true_only_once_or_fail(got_depots, uniq_err_msg + line);
			recgn_depot_section_content(data);
		}
		else if (line == "DISPLAY_DATA_SECTION") {
			proceed_only_if_(_display_data_type != DisplayDataType::NO_DISPLAY);
			turn_true_only_once_or_fail(got_displays, uniq_err_msg + line);
			recgn_display_data_section_content(data);
		}
		else if (line == "EOF")
			reached_eof_flag = true;
		else if (line != "")
			fail("Inst::recgn_data: identifier not recognized: " + line);
	}
	LGN("- " << __PRETTY_FUNCTION__);
}

void Inst::recgn_node_coord_section_content(stringstream& data) {
	_coords.assign(_n, Coords<double>(0.0, 0.0));
	int node_id;
	if (_edge_weight_type == EdgeWeightType::EUC_2D or _edge_weight_type == EdgeWeightType::GEO)
		for (int i = 1; i <= _n; i++) {
			data >> node_id;
			fail_if_(node_id != i, "Error: node_coord_section: node_id != i");
			data >> _coords[i - 1].x;
			data >> _coords[i - 1].y;
			LGN("i " << i << " x " << _coords[i - 1].x << " y " << _coords[i - 1].y);
		}
	else if (_edge_weight_type == EdgeWeightType::ALIEN_FUNCTION and
	_edge_weight_format == EdgeWeightFormat::ALIEN_EUC_2D and
	_node_coord_type    == NodeCoordType::TWOD_COORDS)
		for (int i = 1; i <= _n - 1; i++) {
			data >> node_id;
			fail_if_(node_id != i, "Error: node_coord_section: ALIEN: node_id != i");
			data >> _coords[i].x;
			data >> _coords[i].y;
		}
	else if (_edge_weight_type == EdgeWeightType::NONE)
		fail("Error Inst::recgn_node_coord_section_content: _edge_weight_type == NONE");

	if (_display_data_type == DisplayDataType::COORD_DISPLAY)
		_display_coords.assign(_coords.begin(), _coords.end());
}

void Inst::recgn_edge_weight_section_content(stringstream& data) {
	if (_edge_weight_type == EdgeWeightType::EXPLICIT) {
		if (_edge_weight_format == EdgeWeightFormat::LOWER_ROW) {
//			LOWER ROW: Lower triangular matrix (row-wise without diagonal entries)
			for (int i = 1; i < _n; i++)
				for (int j = 0; j < i; j++) {
					data >> _ds[i][j];
					_ds[j][i] = _ds[i][j];
				}
		}
		else if (_edge_weight_format == EdgeWeightFormat::LOWER_DIAG_ROW) {
//			LOWER_DIAG_ROW: Lower triangular matrix (row-wise including diagonal entries)
			for (int i = 0; i < _n; i++)
				for (int j = 0; j <= i; j++) {
					data >> _ds[i][j];
					_ds[j][i] = _ds[i][j];
				}
		}
		else if (_edge_weight_format == EdgeWeightFormat::UPPER_ROW) {
//			UPPER_ROW: Upper triangular matrix (row-wise without diagonal entries)
			for (int i = 0; i < _n - 1; i++)
				for (int j = i + 1; j < _n; j++) {
					data >> _ds[i][j];
					_ds[j][i] = _ds[i][j];
				}
		}
		else if (_edge_weight_format == EdgeWeightFormat::FULL_MATRIX) {
//			FULL_MATRIX: Weights are given by a full matrix
			for (int i = 0; i < _n; i++)
				for (int j = 0; j < _n; j++)
					data >> _ds[i][j];
		}
	}
}

void Inst::recgn_demand_section_content(stringstream& data) {
	LGN("+ " << __PRETTY_FUNCTION__);
	_qs.assign(_n, 0.0);
	int node_id;
	if (_edge_weight_type == EdgeWeightType::EUC_2D or
	_edge_weight_type == EdgeWeightType::EXPLICIT or
	_edge_weight_type == EdgeWeightType::GEO) {
		for (int i = 1; i <= _n; i++) {
			data >> node_id;
			fail_if_(node_id != i, "Error: demand_section: node_id != i");
			data >> _qs[i - 1];
			if (i - 1 != 0)
				proceed_only_if_(_accept_only_int_client_demands and is_int(_qs[i - 1], EPS), "Error: Inst: recgn_demand_section_content: a client demand value is not integer");
		}
	}
	else if (_edge_weight_type == EdgeWeightType::ALIEN_FUNCTION &&
	_edge_weight_format == EdgeWeightFormat::ALIEN_EUC_2D &&
	_node_coord_type    == NodeCoordType::TWOD_COORDS) {
		for (int i = 1; i <= _n - 1; i++) {
			data >> node_id;
			fail_if_(node_id != i, "Error: demand_section: ALIEN: node_id != i");
			data >> _qs[i];
			proceed_only_if_(_accept_only_int_client_demands and is_int(_qs[i - 1], EPS), "Error: Inst: recgn_demand_section_content: a client demand value is not integer");
		}
	}
	else if (_edge_weight_type == EdgeWeightType::NONE) {
		fail("Error Inst::recgn_demand_section_content: _edge_weight_type == NONE");
	}
	LGN("- " << __PRETTY_FUNCTION__);
}

void Inst::recgn_depot_section_content(stringstream& data) {
	int node_id;
	if (_edge_weight_type == EdgeWeightType::EUC_2D or
	_edge_weight_type == EdgeWeightType::EXPLICIT or
	_edge_weight_type == EdgeWeightType::GEO) {
		data >> node_id;
		while (node_id != -1) {
			_depots.push_back(node_id - 1);
			data >> node_id;
		}
	}
	else if (_edge_weight_type == EdgeWeightType::ALIEN_FUNCTION and
	_edge_weight_format == EdgeWeightFormat::ALIEN_EUC_2D and
	_node_coord_type    == NodeCoordType::TWOD_COORDS) {
		_depots.push_back(0);
		data >> _coords[0].x >> _coords[0].y;
		int minus_one;
		data >> minus_one;
		fail_if_(minus_one != -1);
	}
	_num_depots = _depots.size();
	_num_customers = _n - _num_depots;
	proceed_only_if_(_num_depots == 1, "_num_depots != 1");
}

void Inst::recgn_display_data_section_content(stringstream& data) {
	int node_id;
	_display_coords.assign(_n, Coords<double>(0.0, 0.0));
	if (_display_data_type == DisplayDataType::TWOD_DISPLAY) {
		if (_edge_weight_type == EdgeWeightType::EUC_2D or
		_edge_weight_type == EdgeWeightType::EXPLICIT or
		_edge_weight_type == EdgeWeightType::GEO) {
			for (int i = 1; i <= _n; i++) {
				data >> node_id;
				fail_if_(node_id != i, "Error: recgn_display_data_section_content: node_id != i");
				data >> _display_coords[i - 1].x;
				data >> _display_coords[i - 1].y;
			}
		}
		else if (_edge_weight_type == EdgeWeightType::ALIEN_FUNCTION and
		_edge_weight_format == EdgeWeightFormat::ALIEN_EUC_2D and
		_node_coord_type    == NodeCoordType::TWOD_COORDS) {
			for (int i = 1; i <= _n - 1; i++) {
				data >> node_id;
				fail_if_(node_id != i, "Error: recgn_display_data_section_content: node_id != i");
				data >> _display_coords[i].x;
				data >> _display_coords[i].y;
			}
		}
	}
	else {
		fail("Error Inst::recgn_display_data_section_content");
	}
}

void Inst::calc_dists() {
	if ((_edge_weight_type == EdgeWeightType::EUC_2D) or
	(_edge_weight_type == EdgeWeightType::ALIEN_FUNCTION and _edge_weight_format == EdgeWeightFormat::ALIEN_EUC_2D))
		for (int i = 0; i < _n - 1; i++)
			for (int j = i + 1; j < _n; j++)
				_ds[i][j] = _ds[j][i] = nintf(sqrt((_coords[j].x - _coords[i].x) * (_coords[j].x - _coords[i].x) +
				                                   (_coords[j].y - _coords[i].y) * (_coords[j].y - _coords[i].y)), _int_dist_measure, _num_dec_places_real_dist);
	else if (_edge_weight_type == EdgeWeightType::GEO) {
		double lat_i, long_i, lat_j, long_j;
		double q1, q2, q3;
		for (int i = 0; i < _n - 1; i++)
			for (int j = i + 1; j < _n; j++) {
//				latitude === x and longitude === y, but the intuition is exchanged
//				i
				lat_i  = geocoord_to_rad(_coords[i].x);
				long_i = geocoord_to_rad(_coords[i].y);
//				j
				lat_j  = geocoord_to_rad(_coords[j].x);
				long_j = geocoord_to_rad(_coords[j].y);

				q1 = cos(long_i - long_j);
				q2 = cos(lat_i  - lat_j );
				q3 = cos(lat_i  + lat_j );

				_ds[i][j] = (int) (RRR * acos(0.5 * ((1.0 + q1) * q2 - (1.0 - q1) * q3)) + 1.0);
				_ds[j][i] = _ds[i][j];
		}
	}
}

void Inst::calc_intern_data() {
	if (_inst_type == InstType::CVRP) {
		for (auto i = 1; i < _n; ++i)
			_sum_qs += _qs[i];

		_frac_lb_k = _sum_qs / _Q;
	}
	else if (_inst_type == InstType::TSP) {
		_frac_lb_k = 1;
	}
	_discrete_lb_k  = (int) ceil(_frac_lb_k);
}

void Inst::calc_and_check_extern_data(
const int extern_k,
const bool override_inst_k,
const double extern_rho,
const double extern_q0,
const bool override_inst_q0) {
	if (_inst_type == InstType::CVRP) {
		if (_k == 0 or override_inst_k) {
			if (extern_k > 0)
				_k = extern_k;
			else
				_k = _discrete_lb_k;
		}
		if (_qs[0] == 0.0 or override_inst_q0) {
			if (extern_rho > 0.0)
				_qs[0] = _Q * extern_rho;
			else if (extern_q0 > 0.0)
				_qs[0] = extern_q0;
		}
	}
	else if (_inst_type == InstType::TSP) {
		_k = 1;
	}
}

void Inst::fill_discrete() {
	if (_inst_type == InstType::CVRP) {
		_discrete_Q = nearest_int(_Q);
		_discrete_qs.assign(_qs.size(), 0);
		_discrete_qs[0] = 0;  // numeric_limits<int>::max();  //!< there is no discrete correspondence to _qs[0]
//		_discrete_qs[0] = nearest_int(_qs[0]);
		for (vector<double>::size_type i = 1; i < _qs.size(); ++i) {
			_discrete_qs[i] = nearest_int(_qs[i]);
			_discrete_sum_qs += _discrete_qs[i];
		}
	}
}

double Inst::get_dist_canonical_route() {
	double z = 0.0;
	for (auto i = 1; i < _n; ++i)
		z += _ds[i - 1][i];
	z += _ds[_n - 1][0];
	return z;
}

const string Inst::as_str() {
	stringstream ss;
	ss << "_id                  " << _id      << '\n'
	   << "_comment             " << _comment << '\n'
	   << "_inst_type           " << from_code<InstType>(_inst_type, InstType_) << '\n'
	   << "_n                   " << _n                 << '\n'
	   << "_num_customers       " << _num_customers     << '\n'
	   << "_num_depots          " << _num_depots        << '\n'
	   << "_Q                   " << _Q                 << '\n'
	   << "_discrete_Q          " << _discrete_Q        << '\n'
	   << "_sum_qs              " << _sum_qs            << '\n'
	   << "_discrete_sum_qs     " << _discrete_sum_qs   << '\n'
	   << "_k                   " << _k                 << '\n'
	   << "_frac_lb_k           " << _frac_lb_k         << '\n'
	   << "_int_lb_k            " << _discrete_lb_k     << '\n'
	   << "_q_factor_NMAN       " << _q_factor_NMAN     << '\n'
	   << "_distance_NMAN       " << _distance_NMAN     << '\n'
	   << "_service_time_NMAN   " << _service_time_NMAN << '\n'
	   << "_edge_weight_type    " << from_code<EdgeWeightType>  (_edge_weight_type,   EdgeWeightType_  ) << '\n'
	   << "_edge_weight_format  " << from_code<EdgeWeightFormat>(_edge_weight_format, EdgeWeightFormat_) << '\n'
	   << "_edge_data_format    " << from_code<EdgeDataFormat>  (_edge_data_format,   EdgeDataFormat_  ) << '\n'
	   << "_node_coord_type     " << from_code<NodeCoordType>   (_node_coord_type,    NodeCoordType_   ) << '\n'
	   << "_display_data_type   " << from_code<DisplayDataType> (_display_data_type,  DisplayDataType_ ) << '\n'
	   << "_int_dist_measure    " << _int_dist_measure << '\n'
	   << "_qs          "    << cntner_to_str<vector<double>>(_qs)             << '\n'
	   << "_discrete_qs "    << cntner_to_str<vector<int>>(_discrete_qs)       << '\n'
	   << "_depots "         << cntner_to_str<vector<int>>(_depots)            << '\n'
	   << "_ds "             << sqr_cntner_to_str<matrix<double>>(_ds)         << '\n'
	   << "_coords         " << cntner_to_str<vector<Coords<double>>>(_coords) << '\n'
	   << "_display_coords " << cntner_to_str<vector<Coords<double>>>(_display_coords);
	return ss.str();
}


// #include <utilppq/DEACTSELLOG>
